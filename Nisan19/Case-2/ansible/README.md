# Case - 2

2.1. DB kullanan bir uygulamayı dockerized edip, nginx üzerinden serve edilmesi 
• DB kullanan bir uygulamanın ansible ile dockerize edilmesi 
• Docker kurulumu ve container ayaga kaldırma islemlerinin ansible ile yapılması 
2.2 İlk adımda dockerize edilen uygulamanın Nginx üzerinden serve edilmesi ve gelen request içerisinde bootcamp=devops header'ı varsa "Hoşgeldin Devops" statik sayfasına yönlenebilmesi 
• Nginx kurulumu ve konfigürasyonu ansible ile yapılması
 	• “Hosgeldin DevOps" adlı bir statik sayfa oluşturularak, gelen request içerisinde bootcamp=devops header'ı varsa bu sayfaya yönlendirilmesi 
Not: Tüm işlemlerin ansible ile konfigüre edilmesi gerekmektedir.

## Başlangıç

Bu yazımızda yukarıdaki maddeleri gerçekleştireceğiz. 
Dockerized edilecek uygulama wordpress olarak belirlenmiştir.
Wordpress web sitesi barındırma alt yapısıdır dolayısıyla içerisinde database kullanmaktadır.
Bu yapımızda 1 adet sunucu yönetici 1 adet sunucu hizmet verici olarak ayarlanacaktır

| Makine Adı    |   Görev                            | İşetim Sistemi |  Ağ ayarları | 
| ------------- |   :-------------:                  | -----:         | -----:       |
| master        |   Ansible yönetim server           | ubuntu 20.4    | 192.168.1.72 |
| wpsrv         |   Wordpress, Mysql(MariaDB), Nginx | ubuntu 20.4    | 192.168.1.74 |

Şeklindedir.
İki sunucu üzerinde de ubuntu 20.4 kurulu olup güncelleştirmeleri yapılmıştır (apt-get update).
İki sunucu üzerine de python kurulmuştur. (ansible yönetebilmek için python kütüphanelerine ihtiyaç duymaktadır.)

## Ansible kurulumu

Öncelik olarak ana makinede ansible kurulumu gerçekleştirilmiştir. Bunun için aşağıdaki komutları kullanabilirsiniz.

``` bat  
sudo apt install software-properties-common && sudo apt-add-repository –yes –update ppa:ansible/ansible && sudo apt install ansible
```
![01](./images/1.png)

Kurulum tamamlandıktan sonra "ansible –version" komutu ile 2.9.20 sürümünün sağlıklı olarak kurulduğunu teyit etmiş oluyoruz.

``` bat  
ansible –version
```
![01](./images/2.png)

Aşağıdaki komutlar ile sunucumuzu günelleyip üzerine open ssh server kurulumu gerçekleştiriyoruz

``` bat  
sudo apt update
sudo apt install openssh-server
```

![01](./images/3.png)

Ana makinemizden karşı taraftaki makinenin ayakta olduğunu anlamak için ansible dizini içinden aşağıdaki komutu koşturarak iletişim doğrulamasını yapalım.

``` bat  
ansible all -m ping -u uuo –ask-pass
```
## Hazırlanan konfigürasyonu dağıtımın

![01](./images/5.png)

ansible-playbook -u uuo -i hosts –ask-pass –ask-become-pass -T 60 wordpress.docker.yml komutu ile ansible aracılığı ile wordress.docker.yml dosyamızdaki config ateşlenmekte.

``` bat  
ansible-playbook -u uuo -i hosts –ask-pass –ask-become-pass -T 60 wordpress.docker.yml
```

![01](./images/6.png)

Görselde dağıtımın tamamlandığı görünmekte artık wpsrv sunucumuzun ip adresine gidip(192.168.1.74) görüntüleyebiliriz.

![01](./images/7.png)

Ek olarak header değiştirme işlemi kullanacağımız için ben postman uygulamasında gerçekleştirdim tüm işlemlerimi.

![01](./images/8.png)

Postman aracı ile istenilen header değerleri olarak bootcamp=devops ayarladığımızda 192.168.1.74 IP adresine HTTP GET isteği gönderildikten sonra sayfamızın Hoş geldin Devops sayfasına yönlendirildiği görülmektedir.

![01](./images/9.png)
