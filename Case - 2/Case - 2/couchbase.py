import uuid

import requests

USERNAME = "ununoktiyum"
PASSWORD = "Aa12345 "

BASE_URL = "http://192.168.1.71:8093/query/service"


class User:
    def __init__(self, name, email):
        self.name = name
        self.email = email


def cikti(response, func_name):
    data = response.json()["results"]
    if len(data):
        print("""
            {} -- > 
            """.format(func_name))
        for i in data:
            get_value = i["user"]
            user = User(name=get_value["name"], email=get_value["email"])
            print("""
                name : {}
                email: {} 
                """.format(user.name, user.email))
    else:
        print("""
            {} -- >
                Kayıt bulunamadı.
            """.format(func_name))
    main()


def getUser():
    email = input("user email : ")
    query = "SELECT * FROM `user` WHERE email='{}' LIMIT 1".format(email)
    response = requests.post(BASE_URL, data={"statement": query}, auth=(USERNAME, PASSWORD))
    cikti(response, email + " kullanıcı detayı")
    main()


def getAllData():
    query = "SELECT * FROM `user`"
    response = requests.post(BASE_URL, data={"statement": query}, auth=(USERNAME, PASSWORD))
    cikti(response, "Tüm kullanıcılar")
    main()


def addUser():
    name = input("name :")
    email = input("email :")

    if name and email:
        query = 'insert into `user` (key,value) values ("{id}",{name:"{name}",email:"{email}"})'.format(id=uuid.uuid4(),
                                                                                                        name=name,
                                                                                                        email=email)

        response = requests.post(BASE_URL, data={"statement": query}, auth=(USERNAME, PASSWORD))

        print("response :",response)
        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde eklendi")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı oluşturulurken bir hata oluştu")
    else:
        print("İsim ve email boş geçilemez")
        addUser()
    main()


def deleteUser():
    email = input("email :")
    if email:
        query = 'delete from `user` where email="{}"'.format(email)
        response = requests.post(BASE_URL, data={"statement": query}, auth=(USERNAME, PASSWORD))
        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde silindi.")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı silinirken bir hata oluştu.")
    else:
        print("E-mail boş geçilemez")
        deleteUser()
    main()


def userUpdate():
    email = input("email :")
    new_name = input("yeni isim :")
    new_email = input("yeni email :")
    if email and new_name and new_email:
        query = 'update `user` set  name="{}" , email="{}" where email="{}"'.format(new_name, new_email, email)
        response = requests.post(BASE_URL, data={"statement": query}, auth=(USERNAME, PASSWORD))
        if response.status_code == 200:
            print("Kullanıcı Başarılı bir şekilde güncellendi")
        elif response.status_code == 400:
            print("hat mesajı : ", response.json()["errors"]["msg"])
        else:
            print("Kullancı güncellenirken bir hata oluştu")
    else:
        print("Boş alanlar var lütfen tüm alanları doldurun")
        userUpdate()
    main()


def main():
    print("""
    Seçenekler
    1- Tüm kullanıcılar
    2- Kullanıcı Detay
    3- Kullanıcı Ekle   
    4- Kullanıcı Düzenle
    5- Kullanıcı Silme
    """)
    try:
        secim = int(input("Seçimiz : "))
        if secim < 1 or secim > 5:
            raise ValueError
        else:
            if secim == 1:
                getAllData()
            elif secim == 2:
                getUser()
            elif secim == 3:
                addUser()
            elif secim == 4:
                userUpdate()
            elif secim == 5:
                deleteUser()
    except ValueError:
        print("Lütfen geçerli bir seçenek seçiniz")
        main()


if __name__ == '__main__':
    main()
