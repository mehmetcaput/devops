# Case - 2

   Birinci soruda kurgulayıp tasarladığınız Couchbase Cluster ‘ı üzerinden veritabanı sistemine
ait bilgileri çeken ve kullanıcıya JSON sonuç döndüren bir Pyhton backend müdülü
hazırlanması gerekmekte. (Bu modülü hazırlarken ilk soruda eksik ya da farklı bir
durumumuz varsa herhangi tek başına ayrı bir Couchbase üzerinden de bunu yapabiliriz.
)
Hazırladığınız python modülü Couchbase REST API uçlarından birisine (aşağıdaki listede
available endpointleri görebiliriz) istek atıp gelen cevabı class üzerinden yeni bir obje türetip
JSON list formatında iletebilirsiniz.


## KOD DETAYLARI

```bash
USERNAME = "ununoktiyum"
PASSWORD = "Aa12345 "

BASE_URL = "http://192.168.1.71:8093/query/service"
```
Bu betikte Global bir kullanıcı adı parole ve kodun geri kalanlarında sorguları iletebileceğim temel bir url tanımladım.




```bash
def main():
    print("""
    Seçenekler
    1- Tüm kullanıcılar
    2- Kullanıcı Detay
    2- Kullanıcı Ekle
    3- Kullanıcı Düzenle
    4- Kullanıcı Silme
    """)
    try:
        secim = int(input("Seçimiz : "))
        if secim < 1 or secim > 4:
            raise ValueError
        else:
            if secim == 1:
                getAllData()
            elif secim == 2:
                getUser()
    except ValueError:
        print("Lütfen geçerli bir seçenek seçiniz")
        main()
```

Program Main clası ile döngüsüne başlamakta bu ekranda kullanıcı yapmak istediği işleme göre bir seçim yapmaktayız.

Kullanıcıların seçimlerine göre gerekli fonksiyon devreye girmekte.

Fonksiyonlar içlerinde bulunan sorguları BASE_URL'e ilgili parametreleri olarak ekleyip post isteği göndermekte.


