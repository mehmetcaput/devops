# Case - 1

   Sıfırdan yazılması planlanan bir projenin alt yapısında dağıtık NoSQL bir veritabanı
gereksinimi oluşmuştur. Bu proje üzerinden hem Key-Value hem de SQL sorguları
ile çalışılması beklenmektedir. Altyapıda konumlandırılacak NoSQL veritabanı sisteminin
yüksek erişilebilir, dağıtık yapıda çalışabilen ve gerektiğinde hızlı olarak ölçeklendirilebilen
bir sistem olması gerekmektedir.
Bu proje alt yapısında kullanılacak NoSQL veritabanı sistemini Couchbase teknolojisi ile
tasarlanması gerekmektedir. 



Bu yapının tasarlanması için NoSQL olarak couchbase teknolojisi kullanılacaktır.


## Docker Kurulum 


Couchbase'i docker container yapısı ile kullanabilmek için sunucumuza docker kuracağız 

![01](./images/1.png)

```bash
sudo apt install docker.io
```


Komutunu kullanabilir. Bu komut Ubuntu üzerinde koşturulmuştur

Kurulum tamamlandıktan sonra 

![01](./images/2.png)

```bash
sudo systemctl start docker
sudo systemctl enable docker
```


Komutları koşturularak sistem docker sistem üzerinde başlatılmıştır

Ardından docker repolarıda bulunan couchbase in son versiyonunu çekmek için

![01](./images/4.png)

```bash
sudo docker pull couchbase
```
Komutu kullanılmıştır


Couchbase imajı çekildikten sonra docker üzerinde container koşturacağız bunun içini
```bash
sudo docker run -d name db -p 8091-8094:8091-8094 -p 11210:11210 couchbase
```
Komutunu kullanıyoruz, bu komut 8091 portundan aldığı verileri 8091-8094 arasında portlara iletmekte ek olarak couchbase servisini 11210 üzerinde tetiklemektedir.

Artık tarayıcımızda localhostumuzun 8091 portuna bağlanarak couchbase arayüzüne erişebiliriz.

![01](./images/5.png)

Görüldüğü üzere couchbase ekranı bizi karşılamaktadır.
Bu ekranda Setup diyerek yeni bir Kurulum , Join diyerek containerımızı başka bir node'a bağlayabiliriz.

Sistemimizde öncesinde bir yapı olmadığı için Setup New Cluster diyerek kurulumumuza başlıyoruz.


![01](./images/6.png)

Bu ekranda bu clusterın kullanıcısı belirlenmektedir.
Kullanıcı adı belirlendikten sonra default değerlerde clusterırımız kurulmuştur.

![01](./images/8.png)

Görülüdğü üzere Master clusterımız aktif bir şekilde ayaktadır.

![01](./images/10.png)

ADD bucket ile case1 adlı bucket oluşturulmuştur.


![01](./images/12.png)

```bash
create primary index on `case1`
```
Bu komutla birinil indexi case1 olarak oluşturuyoruz.

![01](./images/13.png)

```bash
insert into `case1` (key, value) values ("trendyol","bootcamp")
```
Case1 e bir adet data ekliyoruz.

![01](./images/14.png)

```bash
Select * from `case1`
```
İle case1 tablosunu seçip gösteriyoruz, fotoğrafta görüldüğü gibi bu tablo içerisinde bir adım önce oluşturduğumuz değerler görünmekte.

![01](./images/151.png)

Postman aracılığı ile gönderilen post isteğinde yeni oluşturduğum clusterın kullanıcı adları görünmektedir.

